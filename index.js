console.log("Hello World!");

let person = {
  firstName: 'Juan',
  lastName: 'Dela Cruz',
  age: 21,
};

let currentAddress = {
	city: 'Quezon City',
	province: 'Metro Manila',
};

console.log('Hello! I am ' + person.firstName + ' ' + person.lastName + ' ' + person.age + ' years old, and currently living in ' + currentAddress.city + ', ' + currentAddress.province + '.');

// Set age of user
let age = 17;

// Place result of ternary operation in a variable
const oldEnough = (age >= 18) ? "You're qualified to vote!" : "Sorry, You're too young to vote.";

// Print output
console.log(oldEnough);


let month = prompt("Please enter month");
let theResponse;
switch (month) {
	case "1":
		theResponse = "Total number of days for January: 31";
		console.log("Total number of days for January: 31");
		break;
	case "2":
		theResponse = "Total number of days for February: 28";
		console.log("Total number of days for February: 28");
		break;
	case "3":
		theResponse = "Total number of days for March: 31";
		console.log("Total number of days for March: 31");
		break;
	case "4":
		theResponse = "Total number of days for April: 30";
		console.log("Total number of days for April: 30");
		break;
	case "5":
		theResponse = "Total number of days for May: 31";
		console.log("Total number of days for May: 31");
		break;
	case "6":
		theResponse = "Total number of days for June: 30";
		console.log("Total number of days for June: 30");
		break;
	case "7":
		theResponse = "Total number of days for July: 31";
		console.log("Total number of days for July: 31");
		break;
	case "8":
		theResponse = "Total number of days for August: 31";
		console.log("Total number of days for August: 31");
		break;
	case "9":
		theResponse = "Total number of days for September: 30";
		console.log("Total number of days for September: 30");
		break;
	case "10":
		theResponse = "Total number of days for October: 31"
		console.log("Total number of days for October: 31");
		break;
	case "11":
		theResponse = "Total number of days for November: 30";
		console.log("Total number of days for November: 30");
		break;
	case "12":
		theResponse = "Total number of days for December: 31";
		console.log("Total number of days for December: 31");
		break;
	default:
		theResponse = "Invalid input! Please enter the month number between 1-12";
		console.log("Invalid input! Please enter the month number between 1-12");
};

alert(theResponse);


// program to check leap year
function checkLeapYear(year) {

    //three conditions to find out the leap year
    if ((0 == year % 4) && (0 != year % 100) || (0 == year % 400)) {
        console.log(year + ' is a leap year');
    } else {
        console.log(year + ' is not a leap year');
    }
}

// take input
const year = prompt('Enter a year:');

checkLeapYear(year);

var counter=5;
function countdown(counter)
{
  if(counter>0)
  {
      counter--;
      setTimeout(function(){countdown(counter)},1000);
      console.log(counter);
  }
}

countdown(counter);
